// 这是云函数的入口函数，不要改变
// event: 用来接收请求放传递过来的参数的
// context:上下文
// callback: 云函数执行完的结果通过callback 响应回去
// logger：日志
let myHandler = async function (event, context, callback, logger) {
  logger.info(event);

  // do something here
   let random = Math.random().toFixed(6);

  callback({
    code: 0,
    desc: "Success." + random
  });
};

export { myHandler };