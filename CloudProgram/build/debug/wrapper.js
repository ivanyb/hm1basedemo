// Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.

const fs = require('fs');
const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');
const CONTEXT_PREFIX = "x-context-";
const USER_ERROR = 180000;
const contentTypeJson = "application/json";

let invokeApp = express();

let router = express.Router();

let funcEnv = {};

const funs = new Map();

let args = process.argv.slice(2);
if (args.length !== 1 && args.length !== 2) {
    throw new Error(`param error`);
}

load().then(() => {
    // for debug
    console.log('Cloud Functions loaded successfully.');
}).catch((err) => {
    console.log('start function error: ' + err.stack);
    process.exit(1);
});

async function load() {
    http.createServer(invokeApp).listen(args[0], 'localhost');
    console.log('HTTP server created successfully, with the listening port: ' + args[0] + '.');
    loadFunc('kists', 'F:\\boxuegu1\\day01\\src\\CloudProgram\\build\\debug\\cloudfunctions\\kists\\kists.js', 'myHandler');
    loadFunc('idgenerator', 'F:\\boxuegu1\\day01\\src\\CloudProgram\\build\\debug\\cloudfunctions\\idgenerator\\IdGenerator.js', 'myHandler');
    let initEnvs = JSON.parse(fs.readFileSync(args[1], 'utf-8'));
    loadFuncEnv(initEnvs.envs);
}

function loadFunc(functionName, entry, handler) {
    funs.set(functionName, { entry: entry, handler: handler })
    require(entry)
    console.log('The POST request URL of function \'' + functionName + '\' is http://localhost:' + args[0] + '/' + functionName + '/invoke.');
}

function loadFuncEnv(envs) {
    if (envs !== null && envs !== undefined) {
        for (let key in envs) {
            funcEnv[key] = envs[key];
            process.env[key] = envs[key];
        }
    }
}

router.post('/:func/invoke', invokeFunction);

invokeApp.use(bodyParser.json());
invokeApp.use("/", router);

function invokeFunction(req, res) {
    let funcName = req.params.func
    let entry = funs.get(funcName).entry
    let handler = funs.get(funcName).handler

    let context = new Context(req, res);
    context.startTime = +new Date();
    let inLogger = new Logger(context);
    context.inLogger = inLogger;
    setContext(context, handlerRequestHeader(req.headers));
    try {
        let functionModule = require(entry);
        if (functionModule === undefined) {
            throw new Error(`Can't find entry file ${entry}`);
        }
        let handlerFunc = functionModule[handler];
        if (handlerFunc === undefined) {
            throw new Error(`Can't find function name ${handler}`);
        }
        handlerFunc(req.body, context, context.callback, inLogger);
    } catch (err) {
        recover(context, err);
    }
}

function handlerRequestHeader(headers) {
    let eventHeaderMap = new Map();
    for (let [key, value] of Object.entries(headers)) {
        key = key.toLocaleLowerCase();
        if (key.indexOf(CONTEXT_PREFIX) === 0) {
            let keyWithoutPrefix = key.replace(CONTEXT_PREFIX, '');
            let valueMap = new Map();
            value.split(";").forEach((value) => {
                let v = value.split("=");
                if (v.length !== 2) {
                    throw new Error(`header ${v} malformed`)
                }
                valueMap[v[0]] = v[1]
            })
            eventHeaderMap[keyWithoutPrefix] = valueMap;
        }
    }
    return eventHeaderMap;
}

function setContext(context, eventHeaderMap) {
    for (let [key, value] of Object.entries(eventHeaderMap)) {
        key = key.toLocaleLowerCase();
        context[key] = value;
    }
}

let Context = function (req, res) {
    this.res = res;
    let that = this;
    this.callback = function () {
        try {
            responseFromOutput(that, ...arguments);
        } catch (err) {
            recover(that, err);
        }
    };

    this.logger = {
        error: function (message) {
            log.call(that, "ERROR", message);
        },
        warn: function (message) {
            log.call(that, "WARN", message);
        },
        info: function (message) {
            log.call(that, "INFO", message);
        },
        debug: function (message) {
            log.call(that, "DEBUG", message);
        }
    };
    this.env = Object.assign({}, funcEnv);
};

function responseFromOutput() {
    let context = arguments[0];
    let res = context.res;

    if (arguments[1] instanceof FError || arguments[1] instanceof Error) {
        setBody(context, res, arguments[1], arguments[2]);
    } else {
        setBody(context, res, undefined, arguments[1]);
    }
}

function setBody(context, res, err, body) {
    var msg = body ? body : err;
    console.log(JSON.stringify(msg));
    res.status(200).send(msg);
}

function recover(context, err) {
    setBody(context, context.res, new FError(USER_ERROR, "Call handler error: " + err.stack));
}

function log(level, message) {
    console.log(`Level: ${level}, Time: ${new Date().toISOString()}, MSG: ${message}`);
}

Context.prototype.HTTPResponse = Response;
Context.prototype.Error = FError;

function Response(body = null, headers = null, contentType = "text/plain", statusCode = 200, isBase64Encoded = false) {
    this.body = body;
    this.headers = headers;
    this.contentType = contentType;
    this.statusCode = statusCode;
    this.isBase64Encoded = isBase64Encoded;

    if (!isString(this.body)) {
        this.body = JSON.stringify(this.body);
        this.contentType = contentTypeJson;
    }
}

function isString(obj) {
    return typeof obj === "string" || obj instanceof String;
}

function FError(code = 0, message = "") {
    this.code = code;
    this.message = message;
}

class Logger {
    constructor(context) {
        this.context = context;
    }

    info(logEvent) {
        this.context.logger.info(typeof logEvent === "string" ? logEvent : JSON.stringify(logEvent));
    }

    error(logEvent) {
        this.context.logger.error(typeof logEvent === "string" ? logEvent : JSON.stringify(logEvent));
    }

    debug(logEvent) {
        this.context.logger.debug(typeof logEvent === "string" ? logEvent : JSON.stringify(logEvent));
    }

    warn(logEvent) {
        this.context.logger.warn(typeof logEvent === "string" ? logEvent : JSON.stringify(logEvent));
    }
}
